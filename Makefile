###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author @todo Kayla Valera <@todo kvalera@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   @todo 04_FEB_2021
###############################################################################

all: animalfarm

animalfarm: main.o animals.o cat.o
	#$(info You need to write your own Makefile)
	#$(info I know you can do it)
	#$(info for now type gcc -o animalfarm *.c)

main.o: animals.h cat.h
cat.o: cat.h animals.h
animals.o: animals.h cat.h
	
clean:
	rm -f *.o animalfarm
